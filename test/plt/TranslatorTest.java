package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {
	
	private static final String HELLO_WORLD = "hello world";
	
	
	@Test
	public void getPhraseShouldReturnHelloWorld() {
		Translator translator = new Translator(HELLO_WORLD);
		
		String inputStr = translator.getPhrase();
		
		assertEquals(inputStr,HELLO_WORLD);
	}
	
	
	@Test
	public void translationShouldReturnNil()throws PigLatinException {
		Translator translator = new Translator("");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,Translator.NIL);
	}
	
	
	@Test
	public void translationOfAnyShouldReturnAnynay()throws PigLatinException {
		Translator translator = new Translator("any");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"anynay");
	}
	
	
	@Test
	public void translationOfAppleShouldReturnAppleyay()throws PigLatinException {
		Translator translator = new Translator("apple");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"appleyay");
	}
	
	
	@Test
	public void translationOfAskShouldReturnAskay()throws PigLatinException {
		Translator translator = new Translator("ask");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"askay");
	}
	
	
	@Test
	public void translationOfHelloShouldReturnEllohay()throws PigLatinException {
		Translator translator = new Translator("hello");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ellohay");
	}
	
	
	@Test
	public void translationOfKnownShouldReturnOwnknay()throws PigLatinException {
		Translator translator = new Translator("known");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ownknay");
	}
	
	
	@Test
	public void translationPhraseWithSpaceSeparatorTest()throws PigLatinException {
		Translator translator = new Translator(HELLO_WORLD);
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ellohay orldway");
	}
	
	
	@Test
	public void translationPhraseWithDashSeparatorTest()throws PigLatinException {
		Translator translator = new Translator("well-being");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ellway-eingbay");
	}
	
	
	@Test
	public void translationPhraseWithPunctuationSpacedWord()throws PigLatinException {
		Translator translator = new Translator("hello world!");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ellohay orldway!");
	}
	
	
	@Test
	public void translationPhraseWithPunctuationCompositeWord()throws PigLatinException {
		Translator translator = new Translator("well-being!");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ellway-eingbay!");
	}
	
	
	@Test(expected = PigLatinException.class)
	public void translationPhraseShouldReturnException()throws PigLatinException {
		Translator translator = new Translator("[well-being]");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"[ellway-eingbay]");
	}
	
	@Test
	public void translationOfAppleShouldReturnAppleyayUpper()throws PigLatinException {
		Translator translator = new Translator("APPLE");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"APPLEYAY");
	}
	
	@Test
	public void translationOfHelloShouldReturnEllohayTitleCase()throws PigLatinException {
		Translator translator = new Translator("Hello");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"Ellohay");
	}
	
	@Test
	public void translationCompositeWordTitleCase()throws PigLatinException {
		Translator translator = new Translator("Well-being");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"Ellway-eingbay");
	}
	
	@Test
	public void translationCompositeWordUpperCase()throws PigLatinException {
		Translator translator = new Translator("WELL-BEING");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ELLWAY-EINGBAY");
	}
	
	@Test
	public void translationSpacedWordTitleCase()throws PigLatinException {
		Translator translator = new Translator("Hello world");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"Ellohay orldway");
	}
	
	@Test
	public void translationSpacedWordUpperCase()throws PigLatinException {
		Translator translator = new Translator("HELLO WORLD");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"ELLOHAY ORLDWAY");
	}
	
	@Test(expected = PigLatinException.class)
	public void translationShouldReturnAnException()throws PigLatinException {
		Translator translator = new Translator("biRd");
		
		String translateStr = translator.translate();
		
		assertEquals(translateStr,"biRd");
	}
	
}
