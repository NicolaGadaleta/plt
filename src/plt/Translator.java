package plt;

import java.util.ArrayList;



public class Translator {
	
	private String phrase = "";
	
	public static final String NIL = "nil";
	
	private static final String AY_SUFFIX = "ay";
	private static final String NAY_SUFFIX = "nay";
	private static final String YAY_SUFFIX = "yay";
	private static final String VOWELS = "aeiou";
	private static final String PUNCTUATION = ".,;:?!'()";
	private static final String INVALID_PUNCTUATION = "[]{}";
	
	
	
	public Translator(String input) {
		this.phrase = input;
		
	}
	
	
	
	
	public String getPhrase() {
		return this.phrase;
	}
	
	
	private String moveToTheEnd(int startPosition, int startingConsonant) {
		String startingConsonantSubStr =  phrase.substring(startPosition,startingConsonant);
		String remainingSubStr = phrase.substring(startingConsonant,phrase.length());
		
		return remainingSubStr.concat(startingConsonantSubStr);
	}
	
	
	private String readdPunctuation(String str,ArrayList<String> punctuation) {
		String rebuildStr = "";
		String finalStr = "";
		boolean added = false;
		int position = 0;
		
		if(remainingPunctuation(punctuation,position) > 0){
			for(String punct:punctuation){
				
				if(!punct.equals("�") && remainingPunctuation(punctuation,position) > 0 && added) {
					finalStr = rebuildStr.concat(punct);
				}else{	
					added = true;
					rebuildStr = str;
				}	
				position++;
			}
			
		}else{
			finalStr = str;
		}
		
		return finalStr;
	}
	
	
	
	
	private String startWithVowelTranslation() {
		String suffix = "";
		
			if(phrase.endsWith("y")) {
				suffix = NAY_SUFFIX;
			}else if(isVowel(phrase.length()-1)) {
				suffix = YAY_SUFFIX;	
			}else if(!isVowel(phrase.length()-1)) {
				suffix = AY_SUFFIX;
			}
		return phrase.concat(suffix);
	}
	
	
	private String phraseTranslate(String translateStr,int startingConsonant) {
		if(isVowel(0)) {
			translateStr = startWithVowelTranslation();	
		}else if(startingConsonant >= 1) {
			translateStr = moveToTheEnd(0,startingConsonant).concat(AY_SUFFIX);
		}
		return translateStr;
	}
	
	
	private String spacedPhraseTrasformation(ArrayList <String> punctuation,String translateStr,String computeStr)throws PigLatinException {
		removePhrasePunctuation(punctuation);
		
		if (!isPhraseCorrectlyFormatted()){
			throw new PigLatinException("Phrase is incorrectly formatted");
			
		}else {
			phrase = phrase.toLowerCase();
			int startingConsonant = getStartingConsonantNumber();
			
			translateStr = phraseTranslate(translateStr,startingConsonant);
			translateStr = readdPunctuation(translateStr,punctuation);
			computeStr = computeStr.concat(translateStr.concat(" "));
		}
		return computeStr;
	}
	
	
	private String DushedPhraseTransformation(String translateStr,String computeStr)throws PigLatinException {
		ArrayList<String> punctuation;
		String[] dashSeparatedPhrases = getPhrases("-");
		int position = 0;
		
		for(position = 0; position != dashSeparatedPhrases.length; position++) {
			phrase = dashSeparatedPhrases[position];
			
			punctuation = getPunctuation();
			removePhrasePunctuation(punctuation);
			
			if (!isPhraseCorrectlyFormatted()){
				throw new PigLatinException("Phrase is incorrectly formatted");
				
			}else if(isPhraseUpperCase()){
				phrase = phrase.toLowerCase();
				int startingConsonant = getStartingConsonantNumber();
				
				translateStr = phraseTranslate(translateStr,startingConsonant);
				translateStr = readdPunctuation(translateStr,punctuation);
				computeStr = computeStr.concat(translateStr.concat("-"));
				computeStr = computeStr.toUpperCase();
			}else{
				phrase = phrase.toLowerCase();
				int startingConsonant = getStartingConsonantNumber();
				
				translateStr = phraseTranslate(translateStr,startingConsonant);
				translateStr = readdPunctuation(translateStr,punctuation);
				computeStr = computeStr.concat(translateStr.concat("-"));
			}		
		}
		
		computeStr = computeStr.substring(0,computeStr.length()-1);
		return computeStr;
	}
	
	
	private String translateSelector(String translateStr,String computeStr)throws PigLatinException {
		ArrayList <String> punctuation = null;
		
		if(!phrase.contains("-") && !phrase.isBlank()) {
			punctuation = getPunctuation();
			computeStr = spacedPhraseTrasformation(punctuation,translateStr,computeStr);
			
		}else if(phrase.contains("-") && !phrase.isBlank()){
			computeStr = DushedPhraseTransformation(translateStr,computeStr);

		}else{
			computeStr= translateStr;	
		}	
		return computeStr;
	}
	
	
	public String translate()throws PigLatinException{
		String translateStr = NIL;
		String computeStr = "";
		String[] spaceSeparatedPhrases = getPhrases(" ");
		
			
		int position = 0;
		
		for(position = 0; position != spaceSeparatedPhrases.length; position++) {
			phrase = spaceSeparatedPhrases[position];
			
			if(isThereInvalidPunctuation()) {
				throw new PigLatinException("Invalid Punctuation");
			}else{
				if(phrase.isBlank()) {
					computeStr = translateStr;
					
				}else if(isPhraseUpperCase()) {
					computeStr = translateSelector(translateStr,computeStr);
					computeStr = computeStr.toUpperCase();
					
				}else if(isPhraseTitleCase()){
					computeStr = translateSelector(translateStr,computeStr);
					computeStr = computeStr.substring(0,1).toUpperCase() + computeStr.substring(1);
					
				}else{
					computeStr = translateSelector(translateStr,computeStr);	
				}	
			}	
		}
		return computeStr.trim();
	}
	
	
	
	
	private int remainingPunctuation(ArrayList<String> punctuation,int index) {
		int remainingPunctuation = 0;
		
		while(index < punctuation.size()) {
			if(!punctuation.get(index).equals("�")) {
				remainingPunctuation++;
			}
			index++;
		}
		return remainingPunctuation;	
	}
	
	
	private int getStartingConsonantNumber() {
		int startingConsonant = 0;
		int index = 0;
		
		for (index = 0; index != phrase.length(); index++) {
			if(!isVowel(index)) {
				startingConsonant++;
			}else {
				index = phrase.length()-1;
			}
		}
		return startingConsonant;
	}
	
	
	
	
	private String[] getPhrases(String separator){ 
		String[] phrases = null;
		
		phrases = phrase.split(separator);
		
		return phrases;
	}
	
	
	private ArrayList<String> getPunctuation() {
		ArrayList<String>punct = new ArrayList<>();
		int index = 0;
		
		for(char punctuation: PUNCTUATION.toCharArray()) {
			String character = Character.toString(punctuation);
			
			if(phrase.contains(character)) {
				punct.add(index,character);
			}else {
				punct.add(index,"�");
			}
			index ++;
		}
		
		
		return punct; 
	}
		
	
	
	
	private boolean isVowel(int position) {
		return VOWELS.contains(Character.toString(phrase.charAt(position)));
	}
	
	
	private boolean isThereInvalidPunctuation() {
		boolean isThereInvalidPunctuation = false;
		
		for(char invalidPunctuation: INVALID_PUNCTUATION.toCharArray()) {
			String character = Character.toString(invalidPunctuation);
			
			if(phrase.contains(character)) {
				isThereInvalidPunctuation = true;
			}
		}
		
		return isThereInvalidPunctuation;
	}
	
	
	private boolean isPhraseUpperCase() {
		boolean isUpperCase = true;
		
		for(char letter:phrase.toCharArray()) {
			if(!Character.isUpperCase(letter)) {
				isUpperCase = false;
			}
		}
		return isUpperCase;
	}
	
	
	private boolean isPhraseTitleCase() {
		boolean isTitleCase = true;
		int position = 0;
		
		char letter = phrase.charAt(position);
		
		if(Character.isUpperCase(letter)) {
			for(position = 1; position != phrase.length(); position ++) {
				if(!Character.isUpperCase(letter)) {
					isTitleCase = false;
				}
			}
		}else {
			isTitleCase = false;
		}
		
		return isTitleCase;
	}
	
	
	private boolean isPhraseLowerCase() {
		boolean isLowerCase = true;
		
		for(char letter:phrase.toCharArray()) {
			if(!Character.isLowerCase(letter)) {
				isLowerCase = false;
			}
		}
		return isLowerCase;
	}
	
	private boolean isPhraseCorrectlyFormatted() {
		return isPhraseLowerCase() || isPhraseTitleCase() || isPhraseUpperCase();
	}
	
		
	private void removePhrasePunctuation(ArrayList<String> punctuation) {
		for(String punct:punctuation) {
			phrase = phrase.replace(punct, "");
		}
	}
	
	
}
