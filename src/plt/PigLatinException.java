package plt;

public class PigLatinException extends Exception{
	public PigLatinException() {
		super();
	}
	public PigLatinException(String msg) {
		super(msg);
	}
}
